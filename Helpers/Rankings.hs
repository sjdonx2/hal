module Helpers.Rankings where

import Import
import Data.Number.Erf (invnormcdf)
import Data.Time.Clock.POSIX

{- Comment Rankers -}

diffSort :: Int -> Int -> Int
diffSort = (-)

averageSort :: Int -> Int -> Double
averageSort uv dv = uv' / dv' where
                        uv' = realToFrac uv
                        dv' = realToFrac dv

zScore :: Double -> Double
zScore = invnormcdf . (-) 1 . (/2) . (1 -)

confidenceSortLB :: Int -> Int -> Double -> Double
confidenceSortLB pos total conf
    | total == 0 = 0
    | otherwise =
        (phat + z' 2 - z * sqrt ((/total') $ phat * (1-phat) + z' 4)) / (1 + z' 1)
            where
                total' = realToFrac total
                z = zScore conf
                z' i = z * z / (i * total')
                phat = realToFrac pos / total'

{- Story Rankers -}

hotRanking :: Int       -- Upvotes
           -> Int       -- DownVotes
           -> POSIXTime -- Data in seconds (Possible fix) (Note: change possibly in case on 32-bit(?))
           -> Double    -- hot Value (?)
hotRanking uv dv t = order * sign + (realToFrac t / 45000)
    where
        s = uv - dv
        sign = (realToFrac . signum) s
        order = log $ realToFrac $ max (abs s) 1
