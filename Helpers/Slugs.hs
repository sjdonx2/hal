module Helpers.Slugs where

import Import
import qualified Data.Text as T

slugify :: Text -> Slug
slugify = Slug . T.intercalate "_" . T.words
