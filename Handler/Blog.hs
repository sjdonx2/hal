module Handler.Blog where

import Import
import Yesod.Markdown
import Yesod.Auth
import Helpers.Slugs (slugify)
import Data.Time
import Data.Text as T

blogForm :: Form (Text, Markdown)
blogForm = renderBootstrap $ (,)
    <$> areq textField "Title" Nothing
    <*> areq markdownField "Content" Nothing

updateBlogForm :: Maybe Text -> Maybe Markdown -> Form (Text, Markdown)
updateBlogForm t m = renderBootstrap $ (,)
    <$> areq textField "Title" t
    <*> areq markdownField "Content" m

getBlogR :: Handler Html
getBlogR = do
    meuid <- maybeAuth
    (widget, enctype) <- generateFormPost blogForm
    bes <- runDB $ selectList [] [Desc BlogEntryWhen]
    defaultLayout $ do
        setTitle "Blog Entries"
        $(widgetFile "blog")

postBlogR :: Handler ()
postBlogR = do
    ((result,_),_) <- runFormPost blogForm
    case result of
        FormSuccess (t, m) -> do
            now <- liftIO getCurrentTime
            _ <- runDB $ insert $ BlogEntry t m now (slugify t)
            setMessage "Blog entry added"
        _ -> setMessage "Oh god.. Something terrible occured!"
    redirect BlogR

getBlogEntryR :: Slug -> Handler Html
getBlogEntryR slug = do
    meuid <- maybeAuth
    Entity _ be <- runDB $ getBy404 $ UniqueBlogEntrySlug slug
    defaultLayout $ do
        setTitle $ toHtml $ blogEntryTitle be
        $(widgetFile "blog_entry")

getEditBlogEntryR :: Slug -> Handler Html
getEditBlogEntryR slug = do
    authId <- requireAuthId
    Entity _ be <- runDB $ getBy404 $ UniqueBlogEntrySlug slug
    (widget,enctype) <- generateFormPost $ updateBlogForm (Just $ blogEntryTitle be) (Just $ blogEntryText be)
    defaultLayout $ do
        setTitle $ toHtml $ T.concat ["Edit ",blogEntryTitle be]
        $(widgetFile "edit_blog_entry")

postEditBlogEntryR :: Slug -> Handler ()
postEditBlogEntryR slug = do
    ((result,_),_) <- runFormPost $ updateBlogForm Nothing Nothing
    case result of
        FormSuccess (t,m) -> do
            runDB $ updateWhere [BlogEntrySlug ==. slug] [BlogEntryTitle =. t, BlogEntryText =. m]
            setMessage "Blog entry updated"
        _ -> setMessage "Failed to update blog entry"
    redirect $ BlogEntryR slug

postDeleteBlogEntryR :: Slug -> Handler ()
postDeleteBlogEntryR slug = do
    runDB $ deleteBy $ UniqueBlogEntrySlug slug
    setMessage "Blog Entry Deleted"
    redirect BlogR
