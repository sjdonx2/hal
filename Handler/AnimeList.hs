module Handler.AnimeList where

import Import
import Data.Time (getCurrentTime)
import Data.Time.Format.Human

getAnimeListR :: UserId -> Handler Html
getAnimeListR uid = do
    (user, userToAnime) <- runDB $ do
        userVals <- get404 uid
        userToAnime <- selectList [UserToAnimeUserId ==. uid] [] -- [Entity UserToAnime]
        return (userVals, userToAnime)
    utaWithAnime <- joinUserAnime (map entityVal userToAnime)
    t <- liftIO getCurrentTime
    defaultLayout $ do
        setTitle $ toHtml $ show (userIdent user) ++ "Anime List"
        $(widgetFile "animelist")

-- Create a association list of the anime to the user's data
joinUserAnime :: [UserToAnime] -> Handler [(UserToAnime, Anime)]
joinUserAnime utas = do
    animes <- runDB $ mapM (get404 . userToAnimeAnimeId) utas
    return $ zip utas animes

postAnimeListR :: UserId -> Handler Html
postAnimeListR = error "Not yet implemented: postAnimeListR"
