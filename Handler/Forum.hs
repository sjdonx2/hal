module Handler.Forum where

import Import

getForumR :: Handler Html
getForumR = do
    threads <- runDB $ selectList [] [LimitTo 10]
    defaultLayout $ do
        setTitle "Front Page"
        $(widgetFile "forum")

postForumR :: Handler Html
postForumR = error "Not yet implemented: postForumR"

getSubForumR :: SubForum -> Handler Html
getSubForumR sub = do
    threads <- runDB $ selectList [ThreadSubForum ==. sub] [LimitTo 10]
    defaultLayout $ do
        setTitle "SubForum"
        $(widgetFile "subforum")

postSubForumR :: SubForum -> Handler Html
postSubForumR = error "Not yet implemented: postForumR"
