module Handler.Admin where

import Import
import Control.Monad (unless)
import Yesod.Auth
import Helpers.Slugs (slugify)

animeForm :: Form (Text, Int)
animeForm = renderDivs $ (,)
    <$> areq textField "Title" Nothing
    <*> areq intField "Total Episodes" Nothing

requireAdmin :: Handler ()
requireAdmin = do
    Entity _ admin <- requireAuth
    unless (userAdmin admin) $ permissionDenied "You are not an admin"

getAdminR :: Handler Html
getAdminR = do
    requireAdmin
    (widget, enctype) <- generateFormPost animeForm
    defaultLayout $ do
        setTitle "Admin"
        $(widgetFile "admin")

postAdminR :: Handler Html
postAdminR = error "postAdminR not defined"

postAddAnimeR :: Handler ()
postAddAnimeR = do
    ((result, _), _) <- runFormPost animeForm
    case result of
        FormSuccess (t,i) -> do
            _ <- runDB $ insert $ Anime t i (slugify t)
            setMessage "Anime added"
        _ -> setMessage "Dear heavens, somethings amiss!"
    redirect AdminR
