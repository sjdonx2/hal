module Handler.Anime where

import Import
import Data.Time
import Yesod.Auth

getAllAnimeR :: Handler Html
getAllAnimeR = do
    animeE <- runDB $ selectList [] [] :: Handler [Entity Anime]
    let animeVals = map entityVal animeE
    defaultLayout $ do
        setTitle "Index of Anime"
        $(widgetFile "all-anime")

episodeForm :: Form Int
episodeForm = renderBootstrap $ areq intField "Episodes Seen" (Just 0 :: Maybe Int)

getAnimeR :: Slug -> Handler Html
getAnimeR slug = do
    muid <- maybeAuthId
    (widget, enctype) <- generateFormPost episodeForm
    animeVals' <- runDB $ getBy404 $ UniqueSlug slug
    let animeVals = entityVal animeVals'
    defaultLayout $ do
        setTitle $ toHtml $ animeTitle animeVals
        -- addStylesheet $ StaticR css_bootstrap_css
        addStylesheetRemote "//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css"
        $(widgetFile "anime")

getEditAnimeR :: Slug -> Handler Html
getEditAnimeR slug = error "Not yet implemented"

postEditAnimeR :: Slug -> Handler Html
postEditAnimeR slug = error "Not yet implemented"

postAddAnimeToListR :: Slug -> Handler ()
postAddAnimeToListR slug = do
    uid <- requireAuthId
    ((result, _), _) <- runFormPost episodeForm
    case result of
        FormSuccess i -> do
            t <- liftIO getCurrentTime
            runDB $ do
              aid <- getBy404 $ UniqueSlug slug
              res <- insertUnique $ UserToAnime t i (entityKey aid) uid
              case res of
                 Just _ -> setMessage "Anime added"
                 Nothing -> do updateWhere [UserToAnimeAnimeId ==. entityKey aid, UserToAnimeUserId ==. uid] [ UserToAnimeTime =. t, UserToAnimeEpSeen =. i]
                               setMessage "Anime updated"

            redirect $ AnimeR slug
        _ -> do
            setMessage "Error occured"
            redirect $ AnimeR slug

postDeleteFromAnimeListR :: Slug -> Handler ()
postDeleteFromAnimeListR = error "Not yet implemented"
