module Handler.Thread where

import Import

getThreadR :: SubForum -> UserId -> Slug -> Handler Html
getThreadR sub uid slug = do
    (thread, posts) <- runDB $ do
        t <- getBy404 $ UniqueThreadSlug slug
        p <- selectList [PostThreadId ==. entityKey t] []
        return (t,p)
    defaultLayout $ do
        setTitle $ toHtml $ threadTitle $ entityVal thread
        $(widgetFile "thread")

postThreadR :: SubForum -> UserId -> Slug -> Handler Html
postThreadR = error "Not yet implemented: postThreadR"
