{-# LANGUAGE StandaloneDeriving #-}
module ExtraModels where

import Prelude
import Yesod
import Data.Text (Text)
import Database.Persist.TH()

newtype Slug = Slug {unSlug :: Text}
               deriving (Show, Eq, Read, PathPiece)
derivePersistField "Slug"

data VoteType = Down | Up
                 deriving (Show, Eq, Read)
derivePersistField "VoteType"

data SubForum = General | AnimeSub | MangaSub | DramaSub
                    deriving (Show, Eq, Read)
derivePersistField "SubForum"

instance PathPiece SubForum where
    fromPathPiece txt =
        case txt of
            "General" -> Just General
            "Anime"   -> Just AnimeSub
            "Manga"   -> Just MangaSub
            "Drama"   -> Just DramaSub
            _         -> Nothing

    toPathPiece General = "General"
    toPathPiece AnimeSub = "Anime"
    toPathPiece MangaSub = "Manga"
    toPathPiece DramaSub = "Drama"
